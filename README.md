Descrição do template:

1)
Makefile: arquivo usado para compilação do projeto. Compila com $ make e $ make clean limpa os
arquivos auxiliares. Deve-se alterar os dois itens abaixo para cada projeto:
    # Basename for result
    TARGET=templateRelatorioWikki

    # Sources
    SRC=templateRelatorioWikki.tex

2)
templateRelatorioWikki.tex: Arquivo principal do projeto. A parte que é adaptada para cada caso
está descrita no próprio arquivo, basicamente titulo, autor,..., assim como inclusão de arquivos
de capítulos por exemplo, por exemplo:   \include{chapters/Introduction_1}.

3) Em documentBeginEnd encontra-se o arquivo que compõe a capa, agradecimentos e o
arquivos de referencias bibliográficas. Estes são arquivos que mudam para diferentes documentos.

4) No diretório chapters é onde realmente fica o conteúdo do documento, organizados por capítulos.

5) Em Figures estão todas as figuras associadas ao documentos, organizadas por diretorios relativos
a cada capitulo.

6) Em Movies estão todas os vídeos/animações associadas ao documentos, organizados por diretorios 
relativos a cada capitulo.

7) Dentro do diretório style estão os arquivos e a classe de formatação desse template. Não existe
a necessidade de mexer em nada dentro deste diretório, a menos que se queira mexer no estilo padrão
definido para WikkiBr.

8) No diretório generalFormat também estão arquivos que a principio são padrão para todos os
documentos, como convenções e definições de formatação de cabeçalhos de documentos, formatação
usada na lista de símbolos e referencias.
No arquivo Preamble.tex são definidas algumas formatações adicionais e definidas algumas variáveis
comumente usadas.

9) Tem vários arquivos dentro de chapters com conteúdo de texto e que serve como guia para 
criação de capítulos, seções e subseções, inclusão de tabelas e figuras, etc. Pode ser interessante
olhar o conteúdo para ajudar na redação.

O uso de \marginNote ou \maxipage e \minipage por exemplo, estão descritos
no documento base que acompanha o manual, sendo necessário recorrer ao documento em caso de
duvida de como se faz determinada ação.


Sobre notas:
Notas de margem:
 \marginnote{sgsd}
 \marginfig{\home\test.png}
 \marginWarn{sgs}
 \margintips{sgd}
 \marginNote{dgds}
 \marginLinux{dgs}

 Notas de paginas:
 \warning{}
 \note{}
 \linuxnote{}
 \tip{}		\\Dicas!!


 Para usar o minted precisa instalar o pacote: sudo apt-get install python-pygments python-pip
 
 Jovani - 25/07/2017
