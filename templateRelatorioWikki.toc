\babel@toc {portuguese}{}\relax 
\contentsline {chapter}{Lista de S\'imbolos}{1}{section*.2}%
\contentsline {chapter}{\numberline {1}Introdu\IeC {\c c}\IeC {\~a}o}{3}{chapter.1}%
\contentsline {section}{\numberline {1.1}Contextualiza\IeC {\c c}\IeC {\~a}o}{3}{section.1.1}%
\contentsline {subsection}{\numberline {1.1.1}Cen\IeC {\'a}rio cujos c\IeC {\'a}lculos atuais s\IeC {\~a}o suficientemente acurados}{3}{subsection.1.1.1}%
\contentsline {subsection}{\numberline {1.1.2}Cen\IeC {\'a}rio no qual conflu\IeC {\^e}ncia pode impactar na perda de carga}{5}{subsection.1.1.2}%
\contentsline {section}{\numberline {1.2}Objetivo do projeto}{7}{section.1.2}%
\contentsline {chapter}{\numberline {2}Cen\IeC {\'a}rios a serem avaliados}{8}{chapter.2}%
\contentsline {chapter}{\numberline {3}Metodologia}{10}{chapter.3}%
\contentsline {section}{\numberline {3.1}M\IeC {\'e}todos de an\IeC {\'a}lise dos resultados}{11}{section.3.1}%
\contentsline {subsection}{\numberline {3.1.1}M\IeC {\'e}todo da superf\IeC {\'\i }cie}{12}{subsection.3.1.1}%
\contentsline {section}{\numberline {3.2}Modelagem CFD}{22}{section.3.2}%
\contentsline {subsection}{\numberline {3.2.1}Equa\IeC {\c c}\IeC {\~o}es do problema}{22}{subsection.3.2.1}%
\contentsline {subsubsection}{\textbf {$\bullet $ Incompress\IeC {\'\i }vel}}{22}{section*.20}%
\contentsline {subsubsection}{\textbf {$\bullet $ Compress\IeC {\'\i }vel}}{23}{section*.21}%
\contentsline {subsection}{\numberline {3.2.2}Modelagem Num\IeC {\'e}rica}{25}{subsection.3.2.2}%
\contentsline {subsection}{\numberline {3.2.3}Crit\IeC {\'e}rio de converg\IeC {\^e}ncia}{26}{subsection.3.2.3}%
\contentsline {chapter}{\numberline {4}Configura\IeC {\c c}\IeC {\~a}o dos casos}{27}{chapter.4}%
\contentsline {section}{\numberline {4.1}Propriedades analisadas}{27}{section.4.1}%
\contentsline {section}{\numberline {4.2}Condi\IeC {\c c}\IeC {\~o}es de Contorno}{28}{section.4.2}%
\contentsline {section}{\numberline {4.3}Bateria de Casos}{30}{section.4.3}%
\contentsline {chapter}{\numberline {5}\textit {ICV HCM-A 45} da \textit {Baker Hughes}}{33}{chapter.5}%
\contentsline {section}{\numberline {5.1}Geometria}{33}{section.5.1}%
\contentsline {subsection}{\numberline {5.1.1}Simplifica\IeC {\c c}\IeC {\~o}es na geometria}{36}{subsection.5.1.1}%
\contentsline {section}{\numberline {5.2}Malha Computacional}{38}{section.5.2}%
\contentsline {chapter}{\numberline {6}\textit {HS-ICV com Accupulse 45 - Ciclo 8}}{44}{chapter.6}%
\contentsline {section}{\numberline {6.1}Geometria}{44}{section.6.1}%
\contentsline {subsection}{\numberline {6.1.1}Simetria}{46}{subsection.6.1.1}%
\contentsline {section}{\numberline {6.2}Malha Computacional}{46}{section.6.2}%
\contentsline {chapter}{\numberline {7}\textit {ICV Accupulse - On-Off} da \textit {Halliburton}}{54}{chapter.7}%
\contentsline {section}{\numberline {7.1}Geometria}{54}{section.7.1}%
\contentsline {subsection}{\numberline {7.1.1}Simetria}{56}{subsection.7.1.1}%
\contentsline {section}{\numberline {7.2}Malha Computacional}{56}{section.7.2}%
\contentsline {chapter}{\numberline {8}\textit {SSV} da \textit {Halliburton}}{58}{chapter.8}%
\contentsline {section}{\numberline {8.1}Geometria}{58}{section.8.1}%
\contentsline {section}{\numberline {8.2}Malha Computacional}{60}{section.8.2}%
\contentsline {chapter}{\numberline {9}Resultados - Produ\IeC {\c c}\IeC {\~a}o - \textit {HCM-A 45} - Propriedades m\IeC {\'e}dias do \IeC {\'O}leo}{62}{chapter.9}%
\contentsline {section}{\numberline {9.1}Ajuste das perdas de carga pela superf\IeC {\'\i }cie}{62}{section.9.1}%
\contentsline {subsection}{\numberline {9.1.1}Posi\IeC {\c c}\IeC {\~a}o 7 - 82,3\% de abertura}{62}{subsection.9.1.1}%
\contentsline {subsection}{\numberline {9.1.2}Posi\IeC {\c c}\IeC {\~a}o 1 - 2\% de abertura}{67}{subsection.9.1.2}%
\contentsline {chapter}{\numberline {10}Resultados - Inje\IeC {\c c}\IeC {\~a}o - \textit {HCM-A 45}: \IeC {\'A}gua}{72}{chapter.10}%
\contentsline {section}{\numberline {10.1}Ajuste das perdas de carga pela superf\IeC {\'\i }cie}{72}{section.10.1}%
\contentsline {subsection}{\numberline {10.1.1}Posi\IeC {\c c}\IeC {\~a}o 7 - 82,3\% de abertura}{72}{subsection.10.1.1}%
\contentsline {subsection}{\numberline {10.1.2}Posi\IeC {\c c}\IeC {\~a}o 1 - 2\% de abertura}{76}{subsection.10.1.2}%
\contentsline {chapter}{\numberline {11}Resultados - Inje\IeC {\c c}\IeC {\~a}o - \textit {HCM-A 45} G\IeC {\'a}s}{79}{chapter.11}%
\contentsline {section}{\numberline {11.1}Ajuste das perdas de carga pela superf\IeC {\'\i }cie}{79}{section.11.1}%
\contentsline {subsection}{\numberline {11.1.1}Posi\IeC {\c c}\IeC {\~a}o 7 - 82,3\% de abertura}{79}{subsection.11.1.1}%
\contentsline {subsection}{\numberline {11.1.2}Posi\IeC {\c c}\IeC {\~a}o 3 - 6\% de abertura}{84}{subsection.11.1.2}%
\contentsline {chapter}{\numberline {12}Resultados - Produ\IeC {\c c}\IeC {\~a}o - \textit {ICV Accupulse (ciclo 8)} - Propriedades m\IeC {\'e}dias do \IeC {\'O}leo}{87}{chapter.12}%
\contentsline {section}{\numberline {12.1}Ajuste das perdas de carga pela superf\IeC {\'\i }cie}{87}{section.12.1}%
\contentsline {subsection}{\numberline {12.1.1}Posi\IeC {\c c}\IeC {\~a}o 10 - 100\% de abertura}{87}{subsection.12.1.1}%
\contentsline {subsection}{\numberline {12.1.2}Posi\IeC {\c c}\IeC {\~a}o 3 - 2\% de abertura}{92}{subsection.12.1.2}%
\contentsline {chapter}{\numberline {13}Resultados - Inje\IeC {\c c}\IeC {\~a}o - \textit {Accupulse (ciclo 8)} - \IeC {\'A}gua}{97}{chapter.13}%
\contentsline {section}{\numberline {13.1}Ajuste das perdas de carga pela superf\IeC {\'\i }cie}{97}{section.13.1}%
\contentsline {subsection}{\numberline {13.1.1}Posi\IeC {\c c}\IeC {\~a}o 10 - 100\% de abertura}{97}{subsection.13.1.1}%
\contentsline {subsection}{\numberline {13.1.2}Posi\IeC {\c c}\IeC {\~a}o 3 - 2\% de abertura}{101}{subsection.13.1.2}%
\contentsline {chapter}{\numberline {14}Resultados - Inje\IeC {\c c}\IeC {\~a}o - \textit {Accupulse (ciclo8)}: G\IeC {\'a}s}{104}{chapter.14}%
\contentsline {section}{\numberline {14.1}Ajuste das perdas de carga pela superf\IeC {\'\i }cie}{104}{section.14.1}%
\contentsline {subsection}{\numberline {14.1.1}Posi\IeC {\c c}\IeC {\~a}o 10 - 100\% de abertura}{104}{subsection.14.1.1}%
\contentsline {subsection}{\numberline {14.1.2}Posi\IeC {\c c}\IeC {\~a}o 5 - 6\% de abertura}{108}{subsection.14.1.2}%
\contentsline {chapter}{\numberline {15}Resultados - Produ\IeC {\c c}\IeC {\~a}o - \textit {SSV Halliburton} - Propriedades m\IeC {\'e}dias do \IeC {\'O}leo}{111}{chapter.15}%
\contentsline {section}{\numberline {15.1}Ajuste das perdas de carga pela superf\IeC {\'\i }cie}{111}{section.15.1}%
\contentsline {chapter}{\numberline {16}Resultados - Inje\IeC {\c c}\IeC {\~a}o - \textit {SSV Halliburton} - \IeC {\'A}gua}{116}{chapter.16}%
\contentsline {section}{\numberline {16.1}Ajuste das perdas de carga pela superf\IeC {\'\i }cie}{116}{section.16.1}%
\contentsline {chapter}{\numberline {17}Resultados - Inje\IeC {\c c}\IeC {\~a}o - \textit {SSV Halliburton} - G\IeC {\'a}s}{119}{chapter.17}%
\contentsline {section}{\numberline {17.1}Ajuste das perdas de carga pela superf\IeC {\'\i }cie}{119}{section.17.1}%
\contentsline {chapter}{\numberline {18}Resultados - Produ\IeC {\c c}\IeC {\~a}o - \textit {ICV - Halliburton On/Off} - Propriedades m\IeC {\'e}dias do \IeC {\'O}leo}{123}{chapter.18}%
\contentsline {section}{\numberline {18.1}Ajuste das perdas de carga pela superf\IeC {\'\i }cie}{123}{section.18.1}%
\contentsline {chapter}{\numberline {19}Resultados - Inje\IeC {\c c}\IeC {\~a}o - \textit {ICV - Halliburton On/Off}: \IeC {\'A}gua}{127}{chapter.19}%
\contentsline {section}{\numberline {19.1}Ajuste das perdas de carga pela superf\IeC {\'\i }cie}{127}{section.19.1}%
\contentsline {chapter}{\numberline {20}Resultados - Inje\IeC {\c c}\IeC {\~a}o - \textit {ICV - Halliburton On/Off}: G\IeC {\'a}s}{131}{chapter.20}%
\contentsline {section}{\numberline {20.1}Ajuste das perdas de carga pela superf\IeC {\'\i }cie}{131}{section.20.1}%
\contentsline {chapter}{\numberline {21}Conclus\IeC {\~a}o}{135}{chapter.21}%
\contentsline {chapter}{Refer\^encias}{137}{section*.188}%
\contentsline {chapter}{\numberline {A}Insumos}{ii}{appendix.A}%
\contentsline {section}{\numberline {A.1}Geometria}{ii}{section.A.1}%
\contentsline {subsection}{\numberline {A.1.1}\textit {HCM-A 45 - Baker Hughes}}{ii}{subsection.A.1.1}%
\contentsline {subsection}{\numberline {A.1.2}\textit {ICV-HS Accupulse 45 - ciclo 8 - Halliburton}}{iii}{subsection.A.1.2}%
\contentsline {subsection}{\numberline {A.1.3}\textit {HS-ICV On-Off 45 - Halliburton}}{iii}{subsection.A.1.3}%
\contentsline {subsection}{\numberline {A.1.4}\textit {SSV - Halliburton}}{iv}{subsection.A.1.4}%
\contentsline {section}{\numberline {A.2}Propriedades dos fluidos}{iv}{section.A.2}%
\contentsline {section}{\numberline {A.3}Vaz\IeC {\~o}es}{xi}{section.A.3}%
\contentsline {chapter}{\numberline {B}No\IeC {\c c}\IeC {\~o}es e defini\IeC {\c c}\IeC {\~o}es b\IeC {\'a}sicas de medidas de press\IeC {\~o}es}{xiii}{appendix.B}%
\contentsline {chapter}{\numberline {C}Propaga\IeC {\c c}\IeC {\~a}o dos desvios padr\IeC {\~o}es dos m\IeC {\'e}todos de an\IeC {\'a}lise}{xv}{appendix.C}%
\contentsline {chapter}{\numberline {D}Estudo da utiliza\IeC {\c c}\IeC {\~a}o da condi\IeC {\c c}\IeC {\~a}o de simetria}{xvii}{appendix.D}%
\contentsline {chapter}{\numberline {E}Converg\IeC {\^e}ncia de malha}{xxii}{appendix.E}%
\contentsline {subsection}{\numberline {E.0.1}\textit {HCM-A 45} - Posi\IeC {\c c}\IeC {\~a}o 1 - Abertura de 2\%}{xxii}{subsection.E.0.1}%
\contentsline {subsection}{\numberline {E.0.2}\textit {HCM-A 45} - Posi\IeC {\c c}\IeC {\~a}o 7 - Abertura de 82,3\%}{xxii}{subsection.E.0.2}%
\contentsline {subsection}{\numberline {E.0.3}\textit {ICV Halliburton - Accupulse 45 -ciclo 8} - Posi\IeC {\c c}\IeC {\~a}o 3 - Abertura de 2\%}{xxiii}{subsection.E.0.3}%
\contentsline {subsection}{\numberline {E.0.4}\textit {ICV Halliburton - Accupulse 45 -ciclo 8} - Posi\IeC {\c c}\IeC {\~a}o 10 - Abertura de 100\%}{xxiii}{subsection.E.0.4}%
\contentsline {section}{\numberline {E.1}Converg\IeC {\^e}ncia de Malhas}{xxiv}{section.E.1}%
\contentsline {chapter}{\numberline {F}Varia\IeC {\c c}\IeC {\~a}o da configura\IeC {\c c}\IeC {\~a}o num\IeC {\'e}rica - estudo de impacto}{xxvii}{appendix.F}%
\contentsline {chapter}{\numberline {G}Compara\IeC {\c c}\IeC {\~a}o entre Algortimos de Acoplamento}{xxxi}{appendix.G}%
\contentsline {chapter}{\numberline {H}Resultados dos campos de velocidade e press\IeC {\~a}o}{xxxiii}{appendix.H}%
\contentsline {section}{\numberline {H.1}Resultados dos campos de velocidade e press\IeC {\~a}o para \textit {HCM-A 45 - Baker Hughes} na produ\IeC {\c c}\IeC {\~a}o de \IeC {\'o}leo}{xxxiii}{section.H.1}%
\contentsline {subsection}{\numberline {H.1.1}Campos de velocidade}{xxxiii}{subsection.H.1.1}%
\contentsline {subsection}{\numberline {H.1.2}Campos de press\IeC {\~a}o}{xxxv}{subsection.H.1.2}%
\contentsline {section}{\numberline {H.2}Resultados dos campos de velocidade e press\IeC {\~a}o para \textit {HCM-A 45 - Baker Hughes} na inje\IeC {\c c}\IeC {\~a}o de \IeC {\'a}gua}{xxxviii}{section.H.2}%
\contentsline {subsection}{\numberline {H.2.1}Campos de velocidade}{xxxviii}{subsection.H.2.1}%
\contentsline {subsection}{\numberline {H.2.2}Campos de press\IeC {\~a}o}{xxxviii}{subsection.H.2.2}%
\contentsline {section}{\numberline {H.3}Resultados dos campos de velocidade e press\IeC {\~a}o para \textit {HCM-A 45 - Baker Hughes} na inje\IeC {\c c}\IeC {\~a}o de g\IeC {\'a}s}{xli}{section.H.3}%
\contentsline {subsection}{\numberline {H.3.1}Campos de velocidade}{xli}{subsection.H.3.1}%
\contentsline {subsection}{\numberline {H.3.2}Campos de press\IeC {\~a}o}{xli}{subsection.H.3.2}%
\contentsline {section}{\numberline {H.4}Resultados dos campos de velocidade e press\IeC {\~a}o para \textit {ICV Accupulse (ciclo 8)} na produ\IeC {\c c}\IeC {\~a}o de \IeC {\'o}leo}{xliii}{section.H.4}%
\contentsline {subsection}{\numberline {H.4.1}Campos de velocidade}{xlv}{subsection.H.4.1}%
\contentsline {subsection}{\numberline {H.4.2}Campos de Press\IeC {\~a}o}{xlv}{subsection.H.4.2}%
\contentsline {section}{\numberline {H.5}Resultados dos campos de velocidade e press\IeC {\~a}o para \textit {ICV Accupulse (ciclo 8)} na inje\IeC {\c c}\IeC {\~a}o de \IeC {\'a}gua}{xlviii}{section.H.5}%
\contentsline {subsection}{\numberline {H.5.1}Campos de velocidade}{xlix}{subsection.H.5.1}%
\contentsline {subsection}{\numberline {H.5.2}Campos de press\IeC {\~a}o}{xlix}{subsection.H.5.2}%
\contentsline {section}{\numberline {H.6}Resultados dos campos de velocidade e press\IeC {\~a}o para \textit {ICV Accupulse (ciclo 8)} na inje\IeC {\c c}\IeC {\~a}o de g\IeC {\'a}s}{lii}{section.H.6}%
\contentsline {subsection}{\numberline {H.6.1}Campos de velocidade}{lii}{subsection.H.6.1}%
\contentsline {subsection}{\numberline {H.6.2}Campos de press\IeC {\~a}o}{lii}{subsection.H.6.2}%
\contentsline {chapter}{\numberline {I}Manual para uso do Power BI}{lvi}{appendix.I}%
\contentsline {section}{\numberline {I.1}Primeira se\IeC {\c c}\IeC {\~a}o do relat\IeC {\'o}rio ("Informa\IeC {\c c}\IeC {\~o}es gerais")}{lvi}{section.I.1}%
\contentsline {section}{\numberline {I.2}Segunda se\IeC {\c c}\IeC {\~a}o do relat\IeC {\'o}rio ("Informa\IeC {\c c}\IeC {\~o}es ajuste")}{lviii}{section.I.2}%
\contentsline {section}{\numberline {I.3}Interpolador}{lx}{section.I.3}%
\contentsline {section}{\numberline {I.4}Ap\IeC {\^e}ndice}{lxi}{section.I.4}%
