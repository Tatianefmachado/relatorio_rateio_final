\chapter{Introdução}
\label{sec:intro}

O presente documento tem como objetivo apresentar a evolução do projeto relacionado ao rateio e confluência de vazões. Neste trabalho estarão expostos estudos que foram realizados com o objetivo de fornecer uma fundamentação para que se compreenda o desenvolvimento e os resultados que serão demonstrados. 

\section{Contextualização}

O projeto em questão surge de uma necessidade associada à gestão de produtividade de um poço de petróleo, por meio da previsão dos fluxos envolvidos em cada uma das zonas. Para isso podem existir \textit{softwares} específicos que auxiliam o projetista. A problemática surge ao constatar que, em determinadas situações os cálculos realizados por esses \textit{softwares} possuem algumas imprecisões quando comparados aos dados de campo, de modo a deixar o projetista com algumas limitações no projetos de poços.

Uma hipótese levantada associa o desvio observado nos cálculos a um fenômeno específico denominado confluência dos fluxos. Essa dinâmica ficará mais clara nas subseções a seguir:

\subsection{Cenário cujos cálculos atuais são suficientemente acurados}

O esquema a seguir mostra um cenário em que os cálculos implementados atualmente apresentam uma acurácia aceitável, implicando em projetos com uma previsibilidade eficiente da produtividade do poço.

A Figura \ref{fig:santos} mostra justamente o esquemático de um poço denominado UO-BS (Bacia de Santos).

\begin{figure}[H]
    \centering
    \includegraphics[width = 0.7\linewidth]{Figuras/introducao/uo-bs.png}
    \caption{Esquemático do poço UO-BS.}
    \label{fig:santos}
\end{figure}
\begin{itemize}
    \item [] \note{PDG é a sigla para o sensor de pressão no fundo do poço (do inglês, \textit{Pressure Downhole Gauge})}
\end{itemize}
No cenário onde apenas a \textit{Downhole Interval Control Valve}(ICV) inferior está aberta (denominada ICV-1) o fluxo entra da região anular para a coluna apenas por essa válvula. Nesta situação, os dados de campo são condizentes, com determinada margem de erro, com os cálculos do projetista, ou seja, as perdas de cargas reais da ICV-1 são capturadas pelos modelos empregados. O mesmo ocorre ao se comparar os dados de campo obtidos pelo PDG-2 com os valores previstos em projeto, que consistem basicamente da perda de carga da ICV-1 somada a uma perda de carga associada à coluna.

No caso com apenas a ICV-2 aberta, o fluxo passa apenas por essa válvula e os dois PDGs acabam analisando apenas o fluido estagnado na coluna.

O último cenário possível para essa configuração do poço UO-BS, com duas zonas produtoras, equivale ao caso em que ambas as ICVs encontram-se abertas. Para esta situação os dados de campo obtidos pelos PDGs mostram, novamente a  acurácia desejada nos cálculos empregados.

\subsection{Cenário no qual confluência pode impactar na perda de carga}

Um cenário em que a confluência dos fluxos pode afetar de forma mais intensa as perdas de carga do sistema seria, por exemplo, um poço do campo de Búzios (também localizado na Bacia de Santos). Por serem poços da região do pré-sal, possuem mais zonas produtoras, e consequentemente mais zonas onde ocorrem as confluências dos fluxos.

\begin{figure}[H]
\centering
    \begin{subfigure}[a]{0.47\textwidth}
        \includegraphics[width=\linewidth]{Figuras/introducao/3ICVS_open.png}
        \caption{3 ICVs abertas.}
        \label{fug:3open}
    \end{subfigure}
    \hfill
    \begin{subfigure}[a]{0.47\textwidth}
        \includegraphics[width=\linewidth]{Figuras/introducao/ICV-1_closed.png}
        \caption{ICV-1 fechada.}
        \label{fig:1_closed}
    \end{subfigure}
    
    \begin{subfigure}[b]{0.47\textwidth}
        \includegraphics[width=\linewidth]{Figuras/introducao/ICV-2_closed.png}
        \caption{ICV-2 fechada.}
        \label{fig:2_closed}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.47\textwidth}
        \includegraphics[width=\linewidth]{Figuras/introducao/ICV-3_closed.png}
        \caption{ICV-3 fechada.}
        \label{fig:3_closed}
    \end{subfigure}
    \caption{Esquemático do poço de Búzios.}
    \label{fig:buzios}
\end{figure}

 Com isso, existe uma combinação bem maior de cenários nos quais válvulas estariam abertas ou fechadas, ou ainda parcialmente abertas (válvulas inteligentes multi-posições). A Figura \ref{fig:buzios} anterior mostra o esquemático de um poço de Búzios com algumas variações de possibilidades dos cenários de completação.
 
 Tomando como exemplo o esquemático da Figura \ref{fug:3open} em que as três válvulas encontram-se abertas, os dados de campo do PDG-2 mostram que os cálculos do projetista não capturam de forma acurada a confluência das produções 1 e 2.
 
 Além disso, dados de campo da PDG-3 também não condizem com os cálculos do projetista: não capturam a confluência das produções 2 e 3 na ICV. E o fato de ser ter 3 zonas produtoras e 3 válvulas pode gerar um erro somado não desprezível, contra no máximo as duas zonas de UO-BS.
 
 Como dito anteriormente, a hipótese levantada é de que a confluência dos fluxos, representeada na Figura \ref{fig:conflu} a seguir, seja a causa da diferença entre os resultados de campo e os projetados, de modo que essa confluência insere perdas de carga que não podem ser observadas nos modelos tradicionais.
 
 \begin{figure}[h!]
     \centering
     \begin{subfigure}[b]{0.49\textwidth}
         \centering
         \includegraphics[width= 0.8 \linewidth]{Figuras/introducao/confluencia.png}
         \caption{Esquemático simplificado da confluência dos fluxos em uma válvula qualquer.}
         \label{fig:conflu}
     \end{subfigure}
     \hfill
     \begin{subfigure}[b]{0.49\textwidth}
         \centering
              \includegraphics[width= 0.9 \linewidth]{Figuras/introducao/rateio.png}
         \caption{Esquemático simplificado do rateio de vazão em uma válvula qualquer.}
         \label{fig:rateio}
     \end{subfigure}
        \caption{Fenômenos de confluência e rateio de vazões.}
        \label{fig:ratConflu}
\end{figure}
 

Além do fenômeno da confluência dos fluxos, existe também a situação inversa, aqui denominada de rateio de vazões. Conforme visto anteriormente, a confluência ocorre no cenário de produção, enquanto o rateio ocorre no cenário de injeção, onde os fluidos são injetados na parte superior do poço e passa pelas válvulas em direção às zonas de produção. O fenômeno do rateio é mostrado de forma simplificada na Figura \ref{fig:rateio}. 
 
\section{Objetivo do projeto}

Com o intuito de investigar a hipótese mencionada anteriormente e reduzir os erros observados na previsão das perdas de carga, a WIKKI Brasil juntamente com as companhias de serviço envolvidas propõem analisar tal fenômeno por meio do ferramental CFD (\textit{Computational Fluid Dynamics}).

Para isso, uma série de simulações são realizadas, variando diversos parâmetros, tais como: 
\begin{itemize}
    \item Equipamento
    \item Variação da abertura do equipamento (quando ele é multi-posição)
    \item Variação das propriedades dos fluidos
    \item Tipo de operação (produtor ou injetor)
\end{itemize}

Portanto, o principal objetivo é a obtenção das curvas ou equações que correlacionem as perdas de carga com as variações dos parâmetros mencionados anteriormente, incluindo o efeito da confluência ou do rateio de vazões.

A Figura \ref{fig:equipComplet} apresenta alguns exemplos de equipamentos (válvulas) de completação.

\begin{figure}[h!]
    \centering
    \includegraphics[width=1.1\linewidth]{Figuras/introducao/valves_example.png}
    \caption{Exemplo de equipamentos de completação}
    \label{fig:equipComplet}
\end{figure}
