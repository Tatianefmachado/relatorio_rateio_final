\chapter{Accupulse - Ciclo 8}
\label{sec:Accupulse}

%\vspace{0.5 cm}
% \minitoc

A válvula que será analisada no decorrer deste capítulo é a válvula ICV Accupulse - ciclo 8 da Halliburton. Será analisado por meio do \textit{CFD} o escoamento em diferentes cenários (produção e injeção, com fluidos de propriedades variadas).

\section{Geometria}

Assim como a válvula apresentada no capítulo anterior o grau de deslizamento é o fator que define o quão aberta está a válvula Accupulse. A Tabela \ref{tab:posiAbert_Accupulse} apresenta as nomenclaturas adotadas para cada posição e sua área de abertura.

\begin{table}[h!]
\footnotesize
    \centering
    \caption{Posição \textit{vs.} área de abertura}
    \label{tab:posiAbert_Accupulse}
    \begin{tabular}{|c|c|c|}
    \hline
    Posição &  Área de abertura [in$^2$] & [$\%$] da área aberta \\      \hline
	     3 & 0.218 & 2.0 \\     \hline
         4 & 0.326 & 3.0 \\     \hline
         5 & 0.673 & 6.2 \\     \hline
         6 & 1.106 & 10.1 \\     \hline
         7 & 1.989 & 18.2 \\     \hline
         8 & 4.085 & 37.4 \\     \hline
         9 & 7.910 & 72.3 \\     \hline
         10 & 10.935 & 100.0 \\     \hline
    \end{tabular}
\end{table}


Com o intuito de representar a válvula instalada em um duto, como em uma situação de operação foi criado um CAD em que o domínio sólido consiste de uma região tubular com uma peça deslizante, a qual permite abrir ou fechar a conexão entre o anular e a parte interna da coluna. Na Figura \ref{fig:accupulse8_detail} é apresentado os detalhes da construção da geometria da válvula e suas duas partes (amarela - móvel e vermelha - fixa).

Cabe ressaltar que o CAD foi gerado com base nas cotas fornecidas pela Halliburton e que estão no apêndice de insumos. \textbf{Referenciar}

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.8\textwidth]{Figuras/icv_halli/ICV_halli_esquema.png}
    \caption{Detalhes das comunicações e parte móvel interna da válvula multiposição.}
    \label{fig:accupulse8_detail}
\end{figure}

\begin{figure}[h!]
     \centering
     \begin{subfigure}[b]{0.49\textwidth}
         \centering
         \includegraphics[width=0.4\linewidth]{Figuras/icv_halli/valve_accupulse_detail.png}
         \caption{Detalhe da parte externa da comunicação.}
         \label{fig:accupulse8_chock_ext}
     \end{subfigure}
     \hfill
     \begin{subfigure}[b]{0.49\textwidth}
         \centering
         \includegraphics[width=0.6\textwidth]{Figuras/icv_halli/accupulse_choke_detail.png}
         \caption{Detalhe da parte interna da comunicação.}
         \label{fig:accupulse8_chock_int}
     \end{subfigure}
        \caption{CAD da Acupulse - ciclo 8.}
        \label{valve_orig}
\end{figure}

As Figuras \ref{fig:accupulse8_chock_ext} e \ref{fig:accupulse8_chock_int} apresentam a região do \textit{choke} que, como já citado anteriormente, permite a passagem dos fluxos e concentra a maior perda de carga do sistema observado.


Para criação do domínio fluido a parte sólida da válvula como apresentado nas figuras anteriores é posicionada dentro de um cilindro que representa um poço de 8,5"~de diâmetro. Como representado na Figura \ref{fig:accupulse8_poco}.

\begin{figure}[h!]
    \centering
    \includegraphics[width=1\textwidth]{Figuras/icv_halli/accupulse_com_poco.png}
    \caption{Válvula posicionada no poço de 8,5".}
    \label{fig:accupulse8_poco}
\end{figure}


A Figura \ref{fig:accupulse8_cota} apresenta os raios da entrada da válvula e os raios que formam o anular.

\begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{Figuras/icv_halli/accupulse8_cota.png}
    \caption{Raios das entradas do anular e da coluna.}
    \label{fig:accupulse8_cota}
\end{figure}

%TODO : COLOCAR A ÁREA DE ENTRADA


\begin{itemize}
    \item [] \warning{As áreas que aparecem nas correlações das metodologias apresentadas no Capítulo \ref{sec:metodo} e \textbf{especificas para a ICV HCMA Baker Hughes} são  $A_3$=$A_2$ = 0,007088 [$m^2$] e $A_1$ = X,XXXXXX [$m^2$]}
\end{itemize}

Uma vez posicionada a válvula dentro do poço, pode-se extrair a parte fluida que será utilizada para as simulações \textit{CFD}, conforme possível observar pela Figura \ref{fig:accupulse8_cota}.

O mesmo processo de geração da geometria do domínio fluido é repetido para cada posição de abertura da válvula, uma vez que para realizar essa modificação basta mover a peça deslizante interna da válvula em questão.

\subsection{Simetria}

Para a geometria final do volume de fluido foi utilizada a simetria de 90º do problema, como pode ser visto na Figura \ref{fig:accupulse8_simetria}.

\begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{Figuras/icv_halli/accupulse8_simetria.png}
    \caption{Representação da simetria usada.}
    \label{fig:accupulse8_simetria}
\end{figure}

Por fim, de maneira similar a válvula anteriormente citada o domínio foi estendido em  6 vezes o diâmetro interno ($D_i$) da entrada da coluna, modificação necessária para adequação à norma ANSI/ISA–75.02.01–2008 \cite{isa75}.

%TODO : COLOCAR NÚMERO DE ELEMENTOS DE CADA MALHA

\section{Malha Computacional}
Com as geometrias prontas iniciou-se o processo de geração de malha. Para os casos estudados foram utilizados o software comercial Fluent Meshing.

A Figura \ref{fig:malha_pos3_accupulse} apresenta a malha final utilizada para simulação das baterias de casos incompressíveis com a válvula na posição 3. A confecção dessa malha foi realizada no software Fluent Meshing, o número total de elementos dessa malha é de 2492327 células. 

\begin{figure}[h!]
	\centering
	\includegraphics[width=1\linewidth]{Figuras/icv_halli/malha_pos3_accupulse8.png}
	\caption{Malha computacional da válvula ICV Accupulse - ciclo 8 - posição 3 utilizada para baterias de simulações.}
	\label{fig:malha_pos3_accupulse}
\end{figure}

A Figura \ref{fig:malha_pos4_accupulse} apresenta a malha final utilizada para simulação das baterias de casos incompressíveis com a válvula na posição 4. A confecção dessa malha foi realizada no software Fluent Meshing, o número total de elementos dessa malha é de 1560934 células.

\begin{figure}[h!]
	\centering
	\includegraphics[width=1\linewidth]{Figuras/icv_halli/malha_pos4_accupulse8.png}
	\caption{Malha computacional da válvula ICV Accupulse - ciclo 8 - posição 4 utilizada para baterias de simulações.}
	\label{fig:malha_pos4_accupulse}
\end{figure}

A Figura \ref{fig:malha_pos5_accupulse} apresenta a malha final utilizada para simulação das baterias de casos incompressíveis com a válvula na posição 5. A confecção dessa malha foi realizada no software Fluent Meshing, o número total de elementos dessa malha é de 1398261 células.  

\begin{figure}[h!]
	\centering
	\includegraphics[width=1\linewidth]{Figuras/icv_halli/malha_pos5_accupulse8.png}
	\caption{Malha computacional da válvula ICV Accupulse - ciclo 8 - posição 5 utilizada para baterias de simulações.}
	\label{fig:malha_pos5_accupulse}
\end{figure}

A Figura \ref{fig:malha_pos6_accupulse} apresenta a malha final utilizada para simulação das baterias de casos incompressíveis com a válvula na posição 6. A confecção dessa malha foi realizada no software Fluent Meshing, o número total de elementos dessa malha é de 2440526 células.  

\begin{figure}[h!]
	\centering
	\includegraphics[width=1\linewidth]{Figuras/icv_halli/malha_pos6_accupulse8.png}
	\caption{Malha computacional da válvula ICV Accupulse - ciclo 8 - posição 6 utilizada para baterias de simulações.}
	\label{fig:malha_pos6_accupulse}
\end{figure}


A Figura \ref{fig:malha_pos7_accupulse} apresenta a malha final utilizada para simulação das baterias de casos incompressíveis com a válvula na posição 7. A confecção dessa malha foi realizada no software Fluent Meshing, o número total de elementos dessa malha é de 1493913 células.  

\begin{figure}[h!]
	\centering
	\includegraphics[width=1\linewidth]{Figuras/icv_halli/malha_pos7_accupulse8.png}
	\caption{Malha computacional da válvula ICV Accupulse - ciclo 8 - posição 7 utilizada para baterias de simulações.}
	\label{fig:malha_pos7_accupulse}
\end{figure}

A Figura \ref{fig:malha_pos8_accupulse} apresenta a malha final utilizada para simulação das baterias de casos incompressíveis com a válvula na posição 8. A confecção dessa malha foi realizada no software Fluent Meshing, o número total de elementos dessa malha é de 745531 células.  

\begin{figure}[h!]
	\centering
	\includegraphics[width=1\linewidth]{Figuras/icv_halli/malha_pos8_accupulse8.png}
	\caption{Malha computacional da válvula ICV Accupulse - ciclo 8 - posição 8 utilizada para baterias de simulações.}
	\label{fig:malha_pos8_accupulse}
\end{figure}

A Figura \ref{fig:malha_pos9_accupulse} apresenta a malha final utilizada para simulação das baterias de casos incompressíveis com a válvula na posição 9. A confecção dessa malha foi realizada no software Fluent Meshing, o número total de elementos dessa malha é de 1533409 células.  

\begin{figure}[h!]
	\centering
	\includegraphics[width=1\linewidth]{Figuras/icv_halli/malha_pos9_accupulse8.png}
	\caption{Malha computacional da válvula ICV Accupulse - ciclo 8 - posição 9 utilizada para baterias de simulações.}
	\label{fig:malha_pos9_accupulse}
\end{figure}

A Figura \ref{fig:malha_pos10_accupulse} apresenta a malha final utilizada para simulação das baterias de casos incompressíveis com a válvula na posição 10. A confecção dessa malha foi realizada no software Fluent Meshing, o número total de elementos dessa malha é de 744980 células.  

\begin{figure}[h!]
	\centering
	\includegraphics[width=1\linewidth]{Figuras/icv_halli/malha_pos10_accupulse8.png}
	\caption{Malha computacional da válvula ICV Accupulse - ciclo 8 - posição 10 utilizada para baterias de simulações.}
	\label{fig:malha_pos10_accupulse}
\end{figure}


Outras malhas foram geradas para cada geometria com o objetivo de realizar uma convergência de malha e os resultados dessa convergência são apresentados no Apendice \ref{sec:mesh}.

Assim como as malhas geradas para a válvula citada no capítulo anterior o \code{yPlus} máximo no caso de vazão mais extrema se encontra abaixo do valor de 300.