\chapter{Cenários a serem estudados}\label{chap:cenarios}

Neste capítulo são apresentados as variações estipuladas pelo aditivo com a companhia de serviço.

Para o estudo de rateio/ confluência em questão a priori foi proposto a serem estudados \textbf{6} equipamentos, onde para cada equipamento seriam estudadas \textbf{3} posições (aberturas), \textbf{3} características de fluidos e essas variações seriam feitas para operações de \textbf{produção} e \textbf{injeção}. 

No total seriam $\textbf{2}$ \textbf{condições de operação} x $\textbf{3}$ \textbf{propriedades de fluido} x $\textbf{3}$ \textbf{posições do equipamento} x  \textbf{6} \textbf{equipamentos} = \textbf{108} configurações para serem estudadas. A Figura \ref{fig:cenar} apresenta um fluxograma dessa estimativa de casos a a serem estudados.

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.9\linewidth]{Figuras/cenarios/rateio_cases.png}
    \caption{Estimativa de cenários a serem estudados.}
    \label{fig:cenar}
\end{figure}

Entretanto ao longo do projeto, conforme a disponibilidade de informações geométricas das válvulas, forma analisados um total de 4 válvulas. Dentre elas duas multi posições, sendo a primeira Halliburton Accupulse Ciclo 8 que possui 8 posições e a segunda sendo HCMA-45 da Baker Hughes com 7 posições. Além disso, foram avaliadas cerca de 8 propriedades de fluido distintas para cada posição. Um resumo desse escopo está representado na Tabela \ref{tab:escopo_novo}



\begin{table}[H]
\centering
\begin{tabular}{|c|c|c|}
\hline
\textit{\textbf{Equipamento}} & \textit{\textbf{Válvula}} & \textit{\textbf{Posição}} \\ \hline
\multirow{3}{*}{1}            & ICV Accupulse             & 3                         \\ \cline{2-3} 
                              & ICV Accupulse             & 4                         \\ \cline{2-3} 
                              & ICV Accupulse             & 5                         \\ \hline
\multirow{3}{*}{2}            & ICV Accupulse             & 6                         \\ \cline{2-3} 
                              & ICV Accupulse             & 7                         \\ \cline{2-3} 
                              & ICV Accupulse             & 8                         \\ \hline
\multirow{3}{*}{3}            & ICV Accupulse             & 9                         \\ \cline{2-3} 
                              & ICV Accupulse             & 10                        \\ \cline{2-3} 
                              & ICV HCMA-45               & 1                         \\ \hline
\multirow{3}{*}{4}            & ICV HCMA-45               & 2                         \\ \cline{2-3} 
                              & ICV HCMA-45               & 3                         \\ \cline{2-3} 
                              & ICV HCMA-45               & 4                         \\ \hline
\multirow{3}{*}{5}            & ICV HCMA-45               & 5                         \\ \cline{2-3} 
                              & ICV HCMA-45               & 6                         \\ \cline{2-3} 
                              & ICV HCMA-45               & 7                         \\ \hline
\multirow{2}{*}{6}            & ICV ON-OFF                & Aberta                    \\ \cline{2-3} 
                              & SSV ON-OFF                & Aberta                    \\ \hline
\end{tabular}
\caption{Tabela com novos cenários do projeto.}
\label{tab:escopo_novo}
\end{table}

Neste novo escopo o total de configurações passou para 136 caracterizando mais simulações que o escopo inicial.

\begin{itemize}
    \item [] \warning{Vale ressaltar que nem todos os equipamentos possuem \textbf{3} posições para serem estudados (e.g. SSV's \textit{on/off}) portanto esse número de cenários foram remanejado de forma que os equipamentos que possuem mais posições compensem os que possuem apenas uma condição de abertura.}
\end{itemize}

