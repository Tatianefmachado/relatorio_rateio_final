\chapter{Halliburton - SSV}
\label{sec:SSV}

%\vspace{0.5 cm}
% \minitoc

A válvula que será analisada no decorrer deste capítulo é a válvula SSV da Halliburton. Será analisado por meio do \textit{CFD} o escoamento em diferentes cenários (produção e injeção, com fluidos de propriedades variadas).

\section{Geometria}

Diferente das válvulas apresentadas em capítulos anteriores, nas quais haviam diversas posições de abertura possíveis, na válvula em questão existe apenas a possibilidade de abrir totalmente a válvula.

Com o intuito de representar a válvula instalada em um duto, como em uma situação de operação foi criado um CAD em que o domínio sólido consiste de uma região tubular com uma peça deslizante, a qual permite abrir ou fechar a conexão entre o anular e a parte interna da coluna. Na Figura \textbf{Referenciar} é apresentado os detalhes da construção da geometria da válvula e suas duas partes (amarela - móvel e vermelha - fixa).

Cabe ressaltar que o CAD foi gerado com base nas cotas fornecidas pela Halliburton e que estão no apêndice de insumos. \textbf{Referenciar}

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.8\textwidth]{Figuras/SSV/SSV_esquema.png}
    \caption{Detalhes das comunicações e parte móvel interna da válvula.}
    \label{fig:SSV_detail}
\end{figure}

\begin{figure}[h!]
         \centering
         \includegraphics[width=0.35\linewidth]{Figuras/SSV/SSV_detail.png}
         \caption{Detalhe da parte externa da comunicação.}
         \label{fig:SSV_chock_ext}
\end{figure}

\clearpage

As Figuras \textbf{Referenciar} e \textbf{Referenciar} apresentam a região do \textit{choke} que, como já citado anteriormente, permite a passagem dos fluxos e concentra a maior perda de carga do sistema observado.


Para criação do domínio fluido a parte sólida da válvula como apresentado nas figuras anteriores é posicionada dentro de um cilindro que representa um poço de 8,5"~de diâmetro. Como representado na Figura \textbf{Referenciar}.

\begin{figure}[h!]
    \centering
    \includegraphics[width=1\textwidth]{Figuras/SSV/SSV_com_poco.png}
    \caption{Válvula posicionada no poço de 8,5".}
    \label{fig:SSV_poco}
\end{figure}


A Figura \textbf{Referenciar} apresenta os raios da entrada da válvula e os raios que formam o anular.

\begin{figure}[h!]
    \centering
    \includegraphics[width=1\textwidth]{Figuras/SSV/SSV_cota.png}
    \caption{Raios das entradas do anular e da coluna.}
    \label{fig:SSV_cota}
\end{figure}

%TODO : COLOCAR A ÁREA DE ENTRADA


\begin{itemize}
    \item [] \warning{A área que aparece nas correlações das metodologias apresentadas no Capítulo \ref{sec:metodo} e \textbf{especificas para a SSV da Hallivburton é $A_3$ = 0.007683 [$m^2$]}}
\end{itemize}

Uma vez posicionada a válvula dentro do poço, pode-se extrair a parte fluida que será utilizada para as simulações \textit{CFD}, conforme possível observar pela Figura \ref{fig:accupulse8_cota}.

Por fim, de maneira similar a válvula anteriormente citada o domínio foi estendido em  6 vezes o diâmetro interno ($D_i$) da entrada da coluna, modificação necessária para adequação à norma ANSI/ISA–75.02.01–2008 \cite{isa75}.

\section{Malha Computacional}
Com as geometrias prontas iniciou-se o processo de geração de malha. Para os casos estudados foram utilizados o software comercial Fluent Meshing.

A Figura \textbf{Referenciar} apresenta a malha final utilizada para simulação das baterias de casos incompressíveis com a válvula aberta. A confecção dessa malha foi realizada no software Fluent Meshing, o número total de elementos dessa malha é de 2010980 células. 

\begin{figure}[h!]
	\centering
	\includegraphics[width=1\linewidth]{Figuras/SSV/malha_SSV.png}
	\caption{Malha computacional da válvula SSV utilizada para baterias de simulações.}
	\label{fig:malha_SSV}
\end{figure}

Outras malhas foram geradas para cada geometria com o objetivo de realizar uma convergência de malha e os resultados dessa convergência são apresentados no Apendice \ref{sec:mesh}.

Assim como as malhas geradas para a válvula citada nos capítulos anteriores o \code{yPlus} máximo no caso de vazão mais extrema se encontra abaixo do valor de 300.
