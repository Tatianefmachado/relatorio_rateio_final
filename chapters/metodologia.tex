\chapter{Metodologia}
\label{sec:metodo}

Para se estudar os fenômenos de confluência ou rateio de vazões serão desenvolvidos modelos computacionais, os quais permitem o estudo de diferentes condições de operação. As variáveis que podem ser avaliadas são as aberturas das válvulas, as vazões, as propriedades do fluido e a condição de operação (produção ou injeção), conforme mencionado na seção anterior. Alguns detalhes acerca da modelagem matemática e da metodologia de simulação serão apresentados a seguir.

Após as simulações CFD finalizadas, torna-se possível a obtenção de resultados para as quedas de pressão, as quais são calculadas conforme a norma \textit{ANSI/ISA–75.02.01–2008} \cite{isa75} e seu esquemático pode ser visto na Figura \ref{ponto_medida}. A norma define que os pontos de aferição da pressão são medidos em função do diâmetro interno da válvula e, com isso, a localização dos pontos é função desse parâmetro, determinando uma medida de 2 diâmetros na região anterior ao escoamento pela válvula e 6 diâmetros na região posterior à  válvula. 

\begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{Figuras/introducao/norma_medicao.png}
    \caption{Pontos de medição conforme norma supracitada.}
    \label{ponto_medida}
\end{figure}

Na norma citada está definido, também, o coeficiente da válvula, que correlaciona a vazão, a massa específica do fluido e a perda de carga da válvula. Tal coeficiente, denominado de $C_v$, pode ser calculado como na Equação \ref{eq:Cv} a seguir ao assumir escoamento incompressível com número de \textit{Reynolds} mínimo de 100000 \cite{isa75}. 


\begin{equation}
    C_v = \frac{Q}{N_1} \sqrt{\frac{\sfrac{\rho_1}{\rho_0}}{\Delta P}};
    \label{eq:Cv}
\end{equation}

em que $Q$ é a vazão, $N_1$ é uma constante de conversão de unidades, $\rho_1$ é a massa específica do fluido do escoamento e $\rho_0$ é da água, por fim, $\Delta P$ é a queda de pressão medida.

\section{Métodos de análise dos resultados}\label{sec:metodos_de_analise}

Para as válvulas que serão analisadas ao longo desse projeto se faz necessário a obtenção de duas quedas de pressão diferentes, uma vez em que, nesses equipamentos a passagem do fluxo acontece de uma região anular para a coluna, mas pode existir uma vazão escoando pelo interior da coluna antes dessa conexão entre as regiões (ex.: no cenário de produção). Um esquema simplificado é mostrado na Figura \ref{fig:esquema_prod} a seguir, na qual fica claro que a situação em questão é diferente da que ocorre no especificado na norma, em que o fluido percorre um caminho único, vindo de uma tubulação à montante da válvula, passando pela válvula e saindo no tubo à jusante, sem confluência ou rateio. 

\begin{figure}[H]
    \centering
    \includegraphics[width = \linewidth]{Figuras/metodo/esquema.png}
    \caption{Esquema do poço no cenário de produção.}
    \label{fig:esquema_prod}
\end{figure}

Sendo assim, o esquema das medidas no caso do cenário de produção é mostrado na Figura \ref{fig:esquema_prod_2} a seguir. Nela nota-se que o domínio é estendido para que as distâncias determinadas na norma sejam respeitadas e nota-se também os pontos de medida 1, 2 e 3, que determinam as duas perdas de carga citadas ($\Delta P_{13}$ e $\Delta P_{23}$).

\begin{figure}[H]
    \centering
    \includegraphics[width = \linewidth]{Figuras/metodo/medidas_prod.png}
    \caption{Exemplo da localização das medidas em um poço no cenário de produção.}
    \label{fig:esquema_prod_2}
\end{figure}

Para o caso de injeção, a numeração dos pontos é definida da mesma forma, porém o escoamento começa no fluido de entrada 3 para as saídas 1 e 2, sendo as perdas de carga calculadas como $\Delta P_{31}$ e $\Delta P_{32}$. Outro fator importante de se destacar, é a distância da amostragem, que no caso de injeção é invertida, onde para o 3, a amostragem é feita com cerca de 2 diâmetros ($2 D_i$) antes do início da válvula e para as amostragens em 1 e 2 são 6 diâmetros ($6 D_i$) contados do final da válvula.

Essas perdas de carga são de fácil obtenção nas simulações CFD, no entanto, há o intuito de correlacionar estas perdas com as vazões no sistema, para isso será proposto o método de análise a seguir.

\vspace{0.2cm}
\begin{itemize}
    \item [] \warning{Vale ressaltar que, além do método de análise proposto, os resultados brutos, em termos das perdas de carga mensuradas, também serão fornecidos.}
\end{itemize}


\subsection{Método da superfície}\label{sec:metodosup}

Este método é pautado no balanço de energia do sistema, o qual pode ser expresso como se segue\cite{smith1950introduction}:

\begin{equation}\label{initial}
    \Delta \left[ \dot m \left(h + \frac{V^2}{2} + gZ\right)\right] = \dot Q - \dot W;
\end{equation}

\noindent sendo que $\dot Q$ representa o calor que é gerado ou recebido pelo sistema e $\dot{W}$ é o trabalho das forças cisalhantes, normais de eixo e outros trabalhos presentes no sistema. A variável $V$ é o módulo da velocidade, $g$ a gravidade, $Z$ altura e $\dot{m}$ representa a vazão mássica. Ademais, a variável $h$ representa a entalpia específica e pode ser reescrita como:

\begin{equation}
    h = u + \frac{p_s}{\rho};
\end{equation}

\noindent onde $u$ é a energia interna específica, $p_s$ é a pressão estática e $\rho$ é a densidade. 

Desta forma a Equação \ref{initial} pode ser reescrita como se segue, juntando a energia térmica com as perdas irreversíveis e agregando todos os termos do lado direito em um único termo, que representa as perdas irreversíveis do escoamento. Vale ressaltar que, foram desconsideradas quaisquer contribuições gravitacionais:

\begin{equation}\label{eq:abre}
    \Delta \left[ \dot m \left(\frac{p_s}{\rho} + \frac{V^2}{2}\right)\right] = \underbrace{\dot Q - \dot W - \Delta \left(\dot m u \right).}_{\delta \dot Q_{loss}}
\end{equation}

Sabe-se que para o problema proposto o balanço de massa do sistema se dá de acordo com a Equação \ref{eq:BM}, cujos sub-índices são referentes aos pontos de medida mostrados na Figura \ref{fig:esquema_prod_2}:

\begin{equation}
 \dot m_3 = \dot m_1 + \dot m_2;
 \label{eq:BM}
\end{equation}

Ao considerar um escoamento compressível a equação acima só é válida em termos da vazão mássica e não da vazão volumétrica:

\begin{equation}
    \dot Q_3 \neq \dot Q_1 + \dot Q_2.
\end{equation}

Abrindo então a Equação \ref{eq:abre} com base nos dados do problema, tem-se:

\begin{equation}
    \dot m_3 \left( \frac{p_{s,3}}{\rho_3} + \frac{V_3^2}{2} \right)
    -
    \dot m_1 \left( \frac{p_{s,1}}{\rho_1} + \frac{V_1^2}{2} \right)
    -
    \dot m_2 \left( \frac{p_{s,2}}{\rho_2} + \frac{V_2^2}{2} \right)
    =
    \delta \dot Q_{loss};
\end{equation}

\noindent multiplicando tudo pela densidade em 3 e reorganizando os termos:

\begin{equation} \label{eq:ps_to_p}
    \dot m_3 \left( p_{s,3} + \rho_3 \frac{V_3^2}{2} \right)
    -
    \dot m_1 \left( \frac{\rho_3}{\rho_1} \right) \left( p_{s,1} + \rho_1 \frac{V_1^2}{2} \right)
    -
    \dot m_2 \left( \frac{\rho_3}{\rho_2} \right) \left( p_{s,2} + \rho_2 \frac{V_2^2}{2} \right)
    =
    \rho_3 \ \delta \dot Q_{loss};
\end{equation}

\noindent os termos entre parêntesis $\left( p_{s,i} + \rho_i \frac{V_i^2}{2} \right)$ podem ser interpretados como as pressões de estagnação ($p_i$), ou pressões totais. Simplificando, então, a Equação \ref{eq:ps_to_p} anterior e dividindo todos os termos pela vazão mássica em 3, tem-se a seguinte expressão (\ref{eq:p_initial}):

\begin{equation} \label{eq:p_initial}
    p_3
    -
    \frac{\dot m_1}{\dot m_3} \left( \frac{\rho_3}{\rho_1} \right) p_1
    -
    \frac{\dot m_2}{\dot m_3} \left( \frac{\rho_3}{\rho_2} \right) p_2
    =
    \frac{\rho_3}{\dot m_3} \ \delta \dot Q_{loss};
\end{equation}

Assume-se, a partir de então uma variável denominada razão de fluxo é definida em função das vazões mássicas na Equação \ref{eq:Razao_fluxo}:

\begin{equation}
    \alpha = \frac{\dot m_1}{\dot m_3} \qquad \qquad 1 - \alpha = \frac{\dot m_2}{\dot m_3}.
    \label{eq:Razao_fluxo}
\end{equation}

Utilizando essas definições, pode-se reescrever a Equação \ref{eq:p_initial} anterior como se segue:

\begin{equation} \label{eq:p_initial_alpha}
    p_3
    -
    \alpha \left( \frac{\rho_3}{\rho_1} \right) p_1
    -
    (1-\alpha) \left( \frac{\rho_3}{\rho_2} \right) p_2
    =
    \frac{\rho_3}{\dot m_3} \ \delta \dot Q_{loss}.
\end{equation}

Uma hipótese da modelagem baseia-se em assumir o termo das perdas do lado direito da equação como uma função que depende de um termo quadrático e um termo linear de velocidade, na forma: 

\begin{equation}\label{eq:comLin}
    \frac{\rho_3}{\dot m_3} \delta \dot Q_{loss} = \rho_3 \frac{K V_3^2}{2} + B V_3.
\end{equation}

Outra possibilidade em relação à expressão assumida na Equação \ref{eq:comLin} consiste no termo do lado direito da equação ser representado apenas pelo termo quadrático. 

\begin{equation}\label{eq:semLin}
    \frac{\rho_3}{\dot m_3} \delta \dot Q_{loss} =
    \rho_3 \frac{K V_3^2}{2}.
\end{equation}

Entretanto, cabe ressaltar que o termo linear auxilia nos ajustes das curvas explicados mais adiante e, por isso, optou-se por seguir com sua presença. Sendo assim, pode-se utilizar essa relação e manipular a Equação \ref{eq:p_initial_alpha} como se segue:

\begin{equation}
    p_3 + \alpha p_3 - \alpha p_3 
    -
    \alpha \frac{\rho_3}{\rho_1} p_1 + (1-\alpha) p_3 - (1-\alpha)p_3 
    -
    (1-\alpha) \frac{\rho_3}{\rho_2} p_2 
    = 
    \rho_3 \frac{K V_3^2}{2} + B V_3;
\end{equation}

\noindent chegando-se a:

\begin{equation}
    \alpha \left(p_3 - \frac{\rho_3}{\rho_1} p_1 \right) 
    + 
    (1 - \alpha) \left(p_3 - \frac{\rho_3}{\rho_2} p_2 \right)
    =
    \rho_3 \frac{K V_3^2}{2} + B V_3;
\end{equation}

\noindent onde define-se as quedas de pressões entre os trechos 1-3 e 2-3 como os termos entre parêntesis, resultando em:

\begin{equation}\label{alpha_01}
    \alpha \Delta p_{13} 
    + 
    (1 - \alpha) \Delta p_{23}
    =
    \rho_3 \frac{K V_3^2}{2}  + B V_3;
\end{equation}

\noindent em que:

\begin{equation}
    \Delta p_{13} = p_3 - \frac{\rho_3}{\rho_1} p_1
    \qquad \qquad
    \Delta p_{23} = p_3 - \frac{\rho_3}{\rho_2} p_2.
\end{equation}


A Equação \ref{alpha_01} está em função da razão de fluxo $\alpha$ e da velocidade em 3 ($V_3$), que pode ser reescrita como:

\begin{equation}\label{V3}
    V_3 = \frac{\dot m_3}{\rho_3 A_3} = \frac{\dot m_1 + \dot m_2}{\rho_3 A_3};
\end{equation}

\noindent onde $A_3$ é a área de amostragem no ponto 3 do domínio.

Cabe salientar que as quedas de pressão isoladas também podem ser caracterizadas na forma:

\begin{equation}\label{eq:deltap_13}
     \Delta p_{13}  =
    \rho_3 \frac{K_{13} V_3^2}{2}  + B V_3;
\end{equation}
\noindent e 
\begin{equation}\label{eq:deltap_23}
   \Delta p_{23}  =
    \rho_3 \frac{K_{23} V_3^2}{2}  + B V_3;
\end{equation}

Os coeficientes $K_{13}$, $K_{23}$ e $K$ são definidos em termos das quedas de pressão na válvula obtidas do balanço de energia no sistema. Definindo também um tipo de pressão média ponderada pelas razões de fluxo, o lado esquerdo das Equação \ref{alpha_01} pode ser escrito como:

\begin{equation}
    \Delta p_m 
    = 
    \alpha \Delta p_{13} 
    + 
    (1 - \alpha) \Delta p_{23};
\end{equation}
\noindent e portanto, os coeficientes são relacionados por:
        
        \begin{equation}
            K = \alpha K_{13} + (1-\alpha) K_{23} \;,
            \label{sec:methodology:eq:K_devdd}
        \end{equation}

Expandindo, então, a Equação \ref{alpha_01} chega-se à:

\begin{equation}\label{quaseFinal}
    \Delta p_m = \frac{K}{\rho_3 A_3^2}  \dot m_1^2 + \frac{2 K}{\rho_3 A_3^2}  \dot m_1 \dot m_2  + \frac{K}{\rho_3 A_3^2}  \dot m_2^2 + \frac{B}{\rho_3 A_3} (\dot m_1 + \dot m_2).
\end{equation}
\noindent ou 

\begin{equation}\label{quaseFinal2}
    \Delta p_m = \frac{K}{\rho_3^2 A_3^2}  \dot m_3^2 + \frac{B}{\rho_3 A_3} (\dot m_3).
\end{equation}

Outra hipótese assumida também é que os coeficientes $K$ ou $K_{i}$ possuem uma relação quadrática com $\alpha$. Esta hipótese foi confirmada em alguns resultados preliminares ao representar graficamente a relação de $K_i$ com $\alpha$, um desses testes é observado nas Figuras \ref{fig:pos7K13alpha} e \ref{fig:pos7K23alpha}.

\begin{figure}[h!]
     \centering
     \begin{subfigure}[b]{0.495\textwidth}
         \centering
    \includegraphics[width=1\textwidth]{Figuras/metodo/K13_pos7.png}
    \caption{Variação de $K_{13}$ de acordo com $\alpha$ para posição 7 - abertura de 82,3\%.}
    \label{fig:pos7K13alpha}
     \end{subfigure}
     \hfill
     \begin{subfigure}[b]{0.495\textwidth}
         \centering
    \includegraphics[width=1\textwidth]{Figuras/metodo/K23_pos7.png}
    \caption{Variação de $K_{23}$ de acordo com $\alpha$ para posição 7 - abertura de 82,3\%.}
    \label{fig:pos7K23alpha}
     \end{subfigure}
        \caption{Relação entre os $K's$ para diferentes valores de $\alpha$ na posição 7 da \textit{HCM-A 45} - abertura de 82,3\%.}
\end{figure}


Além disso, o coeficiente linear B proposto também possui uma relação com $\alpha$, conforme mostram as equações a seguir:

\begin{equation}
    K_{i}(\alpha) = a \alpha^2 + b \alpha + c;
\end{equation}
\begin{equation}
    B_{i}(\alpha) = d \alpha + e;
\end{equation}

\noindent onde $i = m, 13 , 23$.

Substituindo as funções de $\alpha$ na equação \ref{quaseFinal}:

\begin{align}
   \Delta p_m = \dfrac{\rho_3}{2} [ a \alpha^2 + b \alpha + c] \dfrac{\dot m_3^2}{\rho_3^2 A_3^2} +  [d\alpha + e] \dfrac{\dot m_3}{\rho_3 A_3} .
   \label{eq:pm_alpha}
\end{align}

Substituindo a definição de $\alpha$ na Equação \ref{eq:pm_alpha} e reorganizando os termos, chega-se a:

\begin{align}
     \Delta p_m = \dfrac{a}{2\rho_3 A_3^2}\dot m_1^2 + b \dfrac{\dot m_1(\dot m_1 + \dot m_2)}{2 \rho_3A_3^2} + c \dfrac{(\dot m_1 + \dot m_2)^2}{2 \rho_3A_3^2} + d \dfrac{\dot m_1}{\rho_3A_3} + e \dfrac{(\dot m_1 + \dot m_2)}{\rho_3 A_3} .
\end{align}

Por fim, chega-se a equação para ajuste final de coeficientes na forma:

\begin{align}
   \Delta p_m =  \dfrac{1}{2\rho_3A_3^2} [\underbrace{(a+b+c)}_{C_0}\dot m_1^2 + \underbrace{(b+2c)}_{C_1}\dot m_1 \dot m_2 + \underbrace{c}_{C_2} \dot m_2^2 + \underbrace{(2dA_3 + 2eA_3)}_{C_3}\dot m_1 + \underbrace{2eA_3}_{C_4}\dot m_2 ] .
\end{align}


Cabe salientar que, qualquer combinação de pressão pode ser representada por essa função de vazões mássicas. Ao passar a área de amostragem e a constante 2.0 para o outro lado, obtém-se a forma final dos coeficientes denominados de $\beta_i$, conforme a Equação \ref{eq:final_compress}.


\begin{align}
   \Delta p_i =  \dfrac{1}{\rho_3} \left[\underbrace{\dfrac{(a+b+c)}{2A_3^2}}_{\beta_0}\dot m_1^2 + \underbrace{\dfrac{(b+2c)}{2A_3^2}}_{\beta_1}\dot m_1 \dot m_2 + \underbrace{\dfrac{c}{2A_3^2}}_{\beta_2} \dot m_2^2 + \underbrace{\dfrac{(2d A_3 + 2e A_3)}{2 A_3^2}}_{\beta_3} \dot m_1 + \underbrace{\dfrac{2e A_3}{2A_3^2}}_{\beta_4}\dot m_2\right]. 
   \label{eq:final_compress}
\end{align}

Assumindo a forma final:


\begin{align}
   \Delta p_i =  \dfrac{1}{\rho_3} \left[\beta_0\dot m_1^2 + \beta_1\dot m_1 \dot m_2 +\beta_2 \dot m_2^2 + \beta_3 \dot m_1 + \beta_4\dot m_2\right] .
   \label{eq:final_DP_com}
\end{align}

Para o caso incompressível, faz-se uso das equações abaixo para simplificar a Equação \ref{eq:final_compress}

\begin{align}
    \dot m = \rho \dot Q;
\end{align}

\begin{align}
    \rho_3 = \rho_2 = \rho_1 = \rho;
\end{align}

Com isso, a forma final para o caso incompressível torna-se:

 \begin{align}
   \Delta p_i =  \rho [\underbrace{\dfrac{(a+b+c)}{2 A_3^2}}_{\beta_0}\dot Q_1^2 + \underbrace{\dfrac{(b+2c)}{2 A_3^2}}_{\beta_1}\dot Q_1 \dot Q_2  + \underbrace{\dfrac{c}{2 A_3^2}}_{\beta_2} \dot Q_2^2 + \beta_3\dot Q_1 + \beta_4\dot Q_2 ]; 
\end{align}

\noindent cuja forma reduzida fica:

\begin{align}
   \Delta p_i = \rho [\beta_0\dot Q_1^2 + \beta_1\dot Q_1 \dot Q_2  +\beta_2 \dot Q_2^2 + \beta_3\dot Q_1 + \beta_4\dot Q_2 ]. 
   \label{eq:final_DP_incom}
\end{align}

A partir de um conjunto de simulações para diferentes vazões é possível gerar uma nuvem de pontos que origina uma superfície na forma da Figura \ref{fig:exemplo_sup}

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.7\textwidth]{Figuras/resultados/pos5/superficie23_pos5.png}
    \caption{Exemplo de superfície ajustada aos coeficientes $\beta_i$.}
    \label{fig:exemplo_sup}
\end{figure}  
    
A partir das equações de perda de carga (\ref{eq:final_DP_incom} e \ref{eq:final_DP_com}) é possível realizar uma regressão linear multivariada, dessa forma, é obtida uma superfície com os coeficientes $\beta_0$, $\beta_1$, $\beta_2$,$\beta_3$ e $\beta_4$, que correlacionam as perdas de carga às vazões dos sistema, conforme exemplificado na Figura \ref{fig:exemplo_sup}. Essas superfícies foram ajustadas através de um código \code{Python} em que foi utilizada a biblioteca \code{sklearn} \cite{sklearn2022}.


No que tange ao procedimento que envolve a regressão multivariada, um resumo é evidenciado na Figura \ref{fig:esquema_regress} a seguir.

\begin{figure}[H]
    \centering
     \includegraphics[width=0.8\textwidth]{Figuras/metodo/metodologia_ajuste.png}
    \caption{Esquema das etapas do ajuste multivariado.}
    \label{fig:esquema_regress}
\end{figure}


Na Figura é possível observar que, a primeira etapa consiste em tratar os dados que foram gerados na simulação, que, juntamente com a forma da função de perda de carga irão originar um conjunto de coeficientes para cada condição estudada.

Primeiramente, é necessário definir a função custo que será minimizada pelo otimizador, cuja forma é baseada em uma função do erro entre o ponto previsto pelo ajuste e o dado de simulação. Existem diversas formas de função custo, porém a utilizada neste trabalho foi a Huber \cite{Huber2009,Owen2006} presente na biblioteca \code{sklearn} do \code{Python}, pois, é uma função própria para tratar dados que contenham pontos discrepantes. Sua forma principal está exposta nas Equações \ref{eq:Huber} e \ref{eq:Huber1}.

\begin{align}\label{eq:Huber}
S = \sum_{i=1}^n \left[ \sigma_h(y_i,y_i^{prev}) + \underbrace{H_e\left(\dfrac{(y_i - y_i^{prev})}{\sigma_h (y_i,y_i^{prev})} \right)}_{\text{Função que muda com outliers}}\sigma_h(y_i,y_i^{prev}) \right] + \underbrace{\alpha \sum_{j=1}^{p_a} \beta_j^2}_{\text{termo associado aos coeficientes} (L_2)}; 
\end{align}

\begin{align}\label{eq:Huber1}
\sigma_h = \sqrt{\dfrac{1}{n} \sum_{i=1}^n  \left[(y_i - y_i^{prev})^2\right]};
\end{align}


\[
  H_e(z) = 
\begin{cases}
    z^2,& \text{se } |z| < \epsilon\\
    2 \epsilon|z| - \epsilon^2, & |z| \geq \epsilon
\end{cases}
\] ;


\begin{align}\label{eq:Error}
Er_{rel} = \text{max}\left( \dfrac{(y_i^{prev} - y_i)}{y_i}\right) \times 100;
\end{align}


Em que, $n$ representa o número de amostras, $p_a$ o número de parâmetros, $\sigma_h$ o desvio padrão, $y_i$ o ponto simulado e $y_i^{prev}$o ponto previsto pelo modelo.Além disso, ela possui um parâmetro denominado $\epsilon$, que é otimizado por um \textit{script} em \code{python} criado pela equipe para retornar a combinação com menor erro máximo relativo (Equação \ref{eq:Error}) possível. Um gráfico que ilustra essa otimização está apresentado na Figura \ref{fig:esquema_otim}:


\begin{figure}[H]
    \centering
    \includegraphics[width=0.6\textwidth]{Figuras/metodo/grafico_Erro.png}
    \caption{Exemplo de gráfico gerado na otimização do $\epsilon$ para minimizar o erro.}
    \label{fig:esquema_otim}
\end{figure}


Em seguida, os parâmetros são otimizados com o objetivo de minimizar a função custo, neste trabalho, foi utilizado o algoritmo de gradiente descendente para esta minimização. Uma vez encontrado o conjunto de parâmetros ótimos para o $\epsilon$ ótimo, é possível avaliar a qualidade do ajuste e gerar uma tabela de dados da perda de carga (Figura \ref{fig:tabelaLatex}). Esta tabela  mostra cada ponto da matriz simulado (preto), o erro relativo (azul), o erro absoluto (vermelho), o valor previsto pelo ajuste e seu desvio padrão (verde). Cabe ressaltar que, tais erros são calculados em relação ao valor médio dos ajustes. Logo, se for considerado o intervalo de confiança de 95\%, deve-se levar em conta o valor obtido $\pm$ duas vezes o desvio padrão.


Além disso, também é gerada uma tabela (Figura \ref{fig:Tabela_menor}) com os desvios de cada coeficiente calculado, para definir sua faixa de aplicabilidade, o $R^2$ do ajuste e o MAPE (\textit{Mean Absolute Percentage Error}) \cite{sklearn2022}.


\begin{figure}[H]
    \centering
    \includegraphics[width=0.9\textwidth]{Figuras/metodo/tabela_maior.png}
    \caption{Exemplo de tabela com os dados de perda de carga (kgf/cm$^2$) após ajuste.}
    \label{fig:tabelaLatex}
\end{figure}


\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{Figuras/metodo/tabela_menor.png}
    \caption{Exemplo de tabela com os parâmetros do ajuste e sua qualidade.}
    \label{fig:Tabela_menor}
\end{figure}

Finalmente, após todos os casos simulados e ajustados será gerado uma base de dados, cujas equações são capazes de prever a perda de carga em função das vazões, considerando tanto o fenômeno da confluência como do rateio. Um resumo da metodologia como um todo está ilustrado na Figura \ref{fig:metodo_completo}:

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{Figuras/metodo/methodology_full.png}
    \caption{Metodologia completa para gerar a base de dados .}
    \label{fig:metodo_completo}
\end{figure}

Dentre as etapas ilustradas, existem duas que necessitam de mais tempo para serem finalizadas, a primeira é a geração de malhas, onde foram necessárias  malhas diferentes para produção, injeção de água e injeção de gás. A segunda consiste na configuração e no tempo de simulação para cada bateria contendo 35 casos cada.


\section{Modelagem CFD}

\subsection{Equações do problema}

Por causa das diferenças relacionadas ao tipo de fluido que passa pela válvula nos diversos casos considerados, é necessária uma análise correspondente ao comportamento de cada escoamento. Por isso, a seguir serão expostas as hipóteses levantadas, tanto para os casos com escoamento de óleo ou água, quanto para os casos com escoamento de gás.  

\subsubsection{\textbf{$\bullet$ Incompressível}}

Tendo em vista os casos de \textbf{produção} de óleo e de \textbf{injeção} de água, as seguintes hipóteses se aplicam:

\begin{itemize}
    \item Estado estacionário;
    \item Monofásico;
    \item Isotérmico;
    \item Incompressível;
\end{itemize}

Para as hipóteses apresentadas o \textit{solver} \code{simpleFoam} \cite{simpleFoam} do \OpenFOAM será utilizado. Este solver em questão resolve o seguinte equacionamento.

\begin{equation}
    \nabla\cdot(\mathbf{U}) = 0; 
\end{equation}\label{ConservationEquation}

\begin{equation}
     \nabla\cdot(\mathbf{U}\mathbf{U^{T}}) = - \nabla p + \nabla \cdot [(\nu +\nu_{t})(\nabla\mathbf{U}+\nabla\mathbf{U^T})];
\end{equation}\label{MomentumEquation}

Como modelo de turbulência para a modelagem de $\nu_t$ foi utilizado o modelo \code{kOmegaSST}. Esse modelo de turbulência foi escolhido baseado na sua capacidade de abordar escoamentos de alto e baixo número de Reynolds. Para garantir a acurácia deste modelo é necessário garantir um $y^+$ abaixo de 300 \cite{Menter1994}. O $y^+$ é uma distância adimensionalizada avaliada em qualquer parede do domínio, seu valor é importante pois define a resolução da camada limite. Devido a isso, é importante que esse número seja menor que o estipulado para o modelo utilizado. 



\subsubsection{\textbf{$\bullet$ Compressível}}

Já para os casos de \textbf{injeção} de gás apenas as seguintes hipóteses podem ser aplicadas:

\begin{itemize}
    \item Estado estacionário;
    \item Monofásico;
\end{itemize}

Para as hipóteses apresentadas o \textit{solver} \code{rhoSimpleFoam} \cite{simpleFoam} do \OpenFOAM será utilizado. Ao contrário dos fluidos incompressíveis, onde a densidade é constante, nos escoamentos compressíveis a densidade varia no espaço e no tempo, ou seja, a densidade torna-se uma propriedade termodinâmica. Para uma mistura pura, a densidade é uma função da pressão e da temperatura. Por isso, em escoamentos compressíveis é necessário resolver a equação da energia (evolução da temperatura), além das equações da quantidade de movimento e da pressão (que é derivada das equações da continuidade). A temperatura é obtida por meio de uma relação termodinâmica após a solução da equação da energia. Depois disso, a densidade pode ser obtida por meio de uma equação de estado.

Em suma, o algoritmo de solução consiste nas seguintes etapas:

\begin{enumerate}
\item Cálculo de uma velocidade intermediária $\bold{u^*}$ por meio da equação da quantidade de movimento. Esta velocidade e a densidade $\rho^*$ não satisfaz a equação de continuidade;

\item Resolução da equação da energia. A compressibilidade $\Psi$ também é atualizada pelo pacote termodinâmico, juntamente com a temperatura e a viscosidade;

\item Cálculo da pressão $p^{n+1}$ que satisfaz a quantidade de movimento e também a equação de continuidade;

\item Corrigir a velocidade e o fluxo de face para que satisfaçam a equação da continuidade;

\item Atualizar a densidade $\rho^{n+1} = \Psi p^{n+1}$;

\item Atualizar as variáveis da turbulência;

\item Se não convergiu, retorna para o início da iteração.
\end{enumerate}

Os detalhes do equacionamento resolvido por esse \textit{solver} podem ser encontrados na documentação do \OpenFOAM \cite{rhoSimpleFoam}. Como modelo de turbulência para a modelagem de $\nu_t$ também foi utilizado o modelo \code{kOmegaSST} \cite{Menter1994}.


\clearpage 
\subsection{Modelagem Numérica}


Adota-se como algoritmo de acoplamento pressão-velocidade para as simulações o algoritmo \code{SIMPLE}~\cite{holzmann}.

Para a configuração dos operadores de discretização um estudo foi realizado variando-se as combinações e verificando o impacto no resultado final das simulações.

Essa variação dava-se principalmente nos operadores divergentes da velocidade. Ao fim os operadores foram definidos de acordo com a Tabela \ref{table03} a seguir, enquanto os estudos realizados variando-se o \textit{setup} encontra-se no Apêndice \ref{apdx:esquemas}

\begin{table}[ht!]
    \setlength{\arrayrulewidth}{1.0pt}
    \centering
        \caption{Discretização dos Operadores Matemáticos.}
    \label{table03}
    \begin{tabular}{c|c}
    \hline
    Operador & Método\\
    \hline
    Derivada Temporal & \code{steadyState}\\
    Operador Gradiente Explícito & \code{pointCellsLeastSquares}\\
    Operador Divergente (\code{U}) & \code{Gauss Upwind}\\
    Operador Divergente (\code{epsilon;omega}) & \code{bounded Gauss upwind}\\
    Operador Laplaciano & \code{Gauss linear corrected} \\
    Operador Gradiente Implícito & \code{corrected} \\
    \hline
    \end{tabular}
\end{table}

\begin{table}[ht!]
    \setlength{\arrayrulewidth}{1.0pt}
    \centering
    \caption{Parâmetros do algoritmo para acoplamento pressão-velocidade.}
    \label{table04}
    \begin{tabular}{c|c}
    \hline
    Parâmetro & Atribuição  \\
    \hline
    Acoplamento \code{p-U} & \code{SIMPLE} \\
    \code{nNonOrthogonalCorrectors} & 4  \\
    \hline
    \end{tabular}
\end{table}

Para a solução dos sistemas lineares resultantes, a Tabela \ref{table05} demonstra o algoritmo utilizado e os seus respectivos parâmetros numéricos \cite{userGuideOF}.

\begin{table}[ht!]
    \setlength{\arrayrulewidth}{1.0pt}
    \centering
    \caption{Parâmetros do algoritmo para solução de sistemas lineares.}
    \label{table05}
    \begin{tabular}{c|c}
    \hline
    Parâmetro & Atribuição  \\
    \hline
    Solver linear para \code{p} & \code{PCG} \\
    \code{preconditioner} & \code{GAMG}   \\
    \code{smoother} & \code{DIC}  \\
    \code{relTol} & \code{1e-4}   \\
    \code{tolerance} & \code{1e-12}   \\
    \hline
    Solver linear para \code{(U|omega|k)} & \code{smoothSolver}  \\
    \code{preconditioner} & \code{symGaussSeidel}   \\
    \code{relTol} & \code{1e-4} \\
    \code{tolerance} & \code{1e-10}   \\
    \hline
    \end{tabular}
\end{table}

\clearpage 

\subsection{Critério de convergência}

A simulação numérica necessita de um critério de convergência definido para que ela possa ser considerada válida. Geralmente isso é feito através dos resíduos, de modo que quando eles chegam em determinado valor, a simulação é finalizada e os resultados são obtidos.

Outro método de acompanhar a convergência da simulação é diretamente a partir das propriedades integradas calculadas pelo \textit{solver}. No caso, essa é a estratégia utilizada para as simulações deste documento.

A pressão na fronteira de entrada de fluxo pelo anular é acompanhada ao longo das iterações da simulação e quando, em uma janela determinada de 200 iterações, a variação dessa variável é menor do que 0,5\% a simulação foi considerada convergida. Nos casos compressíveis, além da pressão, também foi avaliada a média integrada da temperatura ($T$) para considerar o convergência do caso. A Figura \ref{fig:conv_geral} a seguir exemplifica graficamente como essa condição ocorre para a pressão:


\begin{figure}[h!]
    \centering
    \includegraphics[width = 0.6\linewidth]{Figuras/sym_vs_360/press_360.png}
    \caption{Exemplo gráfico do critério de convergência - acompanhamento da pressão.}
    \label{fig:conv_geral}
\end{figure}


O próximo capítulo traz um resumo das configurações dos casos simulados sendo complementado sempre pelo apêndice de insumos \ref{chap:insumos}. Dentro destas configurações estão as propriedades dos fluidos analisados as condições de contorno e as baterias de casos simulados.