\chapter{Noções e definições básicas de medidas de pressões}\label{apdx:basico}

Pressão	é a força aplicada perpendicularmente à superfície de um objeto por unidade de área da superfície sobre a qual ela é aplicada. Ou seja;

\begin{equation}
    p = \frac{F}{A};
\end{equation}

onde $F$ é a força e $A$ a área.

As definições a seguir são realizadas com base em \citeonline{fox2020fox}.

Ao se observar a equação de Bernoulli, vê-se que em cada lado dela têm-se basicamente três termos. A Equação \ref{eq:berna} a seguir mostra isso:

\begin{equation}\label{eq:berna}
    p_1 + \frac{1}{2}\rho v_1^2 + \rho g h_1 = p_2 + \frac{1}{2}\rho v_2^2 + \rho g h_2;
\end{equation}

A pressão estática é a pressão experimentada pela partícula de fluido enquanto ela se move (portanto, é uma espécie de equívoco!) e pode ser associada ao primeiro termo de cada lado da equação de \textit{Bernoulli} acima ($p_1$ e $p_2$). Tem-se também a pressão de estagnação e as pressões dinâmicas, que serão definidas a seguir.

Sabe-se que não há variação de pressão normal às linhas de corrente que percorrem um caminho retilíneo. Sendo assim, é possível medir a pressão estática ao posicionar o medidor de pressão na parede onde as linhas de corrente são retas, como mostrado na Figura \ref{fig:fox1_a}. A tomada de pressão é um pequeno orifício, cuidadosamente perfurado na parede, com seu eixo perpendicular à superfície.

A Figura \ref{fig:fox1_b} mostra como a medição pode ser feita em uma corrente do fluido afastada da parede, ou onde as linhas de corrente são curvas.

Em uso, a seção de medição dos equipamentos mostrados nas figuras deve estar alinhada com a direção do escoamento local. (Pode parecer que a tomada de pressão e os pequenos orifícios permitiriam o escoamento entrar ou sair nas mesmas ou serem arrastadas pelo escoamento principal, mas cada uma dessas tomadas é perfeitamente anexada a um sensor de pressão ou manômetro e não permite a saída do fluido, portanto um , não permitindo que o escoamento seja possível.

\begin{figure}[h!]
\centering
    \begin{subfigure}[a]{0.47\textwidth}
        \includegraphics[width=\linewidth]{Figuras/apdx_basico/fox1.png}
        \caption{Tomada de pressão na parede.}
        \label{fig:fox1_a}
    \end{subfigure}
    \hfill
    \begin{subfigure}[a]{0.47\textwidth}
        \includegraphics[width=\linewidth]{Figuras/apdx_basico/fox2.png}
        \caption{Sonda de pressão estática.}
        \label{fig:fox1_b}
    \end{subfigure}
    \caption{Medição da pressão estática.}
    \label{fig:fox}
\end{figure}

A pressão de estagnação é obtida quando um fluido em escoamento é desacelerado até a velocidade zero por meio de um processo sem atrito. Para escoamento incompressível, a equação de \textit{Bernoulli} pode ser usada para relacionar variações na velocidade e na pressão ao longo de uma linha de corrente nesse processo. Desprezando diferenças de elevação, a Equação \ref{eq:berna} resume-se na Equação \ref{eq:estag} a seguir:

\begin{equation}\label{eq:estag}
    \frac{p}{\rho} + \frac{V^2}{2} = cte;
\end{equation}

Se a pressão estática é $p$ em um ponto do escoamento no qual a velocidade é $V$, então a pressão de estagnação, $p_0$, no qual a velocidade de estagnação, $V_0$, é zero, pode ser obtida a partir da Equação \ref{eq:estag_def} a seguir

\begin{equation}\label{eq:estag_def}
    \frac{p_0}{\rho} + \frac{V
    _0^2}{2} = \frac{p}{\rho} + \frac{V^2}{2};
\end{equation}

onde o segundo termo do lado esquerdo tende a zero por conta da velocidade nula, de modo que a definição final da pressão de estagnação pode ser dada de acordo com a Equação \ref{eq:estag_def_final} a seguir:

\begin{equation}\label{eq:estag_def_final}
    p_0 = p + \frac{1}{2} \rho V^2;
\end{equation}

A Equação \ref{eq:estag_def_final} anterior é um enunciado matemático da definição de pressão de estagnação, válido para escoamento incompressível. O último termo do lado direito da equação é usualmente chamado de pressão dinâmica. Essa equação anterior estabelece que a pressão de estagnação, também chamada de pressão total, é igual à pressão estática mais a pressão dinâmica.
