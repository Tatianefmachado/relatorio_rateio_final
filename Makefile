# Basename for result
TARGET=templateRelatorioWikki

# Sources
SRC=templateRelatorioWikki.tex

# ATTENTION!
# File-extensions to delete recursive from here
EXTENSION=aux toc ind ilg log out lof lot lol bbl blg nlo nls nls backup maf bmt pyg 

SOURCES	= $(wildcard ./chapters/*.tex ./generalFormat/*.tex ./documentBeginEnd/*.tex ./style/*.sty ./style/*.cls )


##### Targets ###############
#############################

all: ${TARGET}.pdf

# PDF
${TARGET}.pdf: ${SRC} ${SOURCES}
	echo  ${SOURCES}
	echo "Running pdflatex..."
	pdflatex -shell-escape ${TARGET} 
	echo "Running bibtex..."
	bibtex ${TARGET}
	echo "Running makeindex (for list of symbols)..."
	makeindex ${TARGET}.nlo -s nomencl.ist -o ${TARGET}.nls
	makeindex ${TARGET}.nlo -s nomencl.ist -o ${TARGET}.ind
	echo "Rerunning pdflatex...."
	pdflatex -shell-escape ${TARGET} 
	pdflatex -shell-escape ${TARGET}
#	pdflatex -shell-escape ${TARGET} 

# Clean
clean:
	for EXT in ${EXTENSION}; \
	do \
	find `pwd` -name \*\.$${EXT} -exec rm -v \{\} \; ;\
	done
	rm -f *~ *.mtc* ${TARGET}.idx
	rm -f ${TARGET}.dvi
	rm -f ${TARGET}.pdf
	rm -f ${TARGET}.ps
	rm -rf _minted-${TARGET}
